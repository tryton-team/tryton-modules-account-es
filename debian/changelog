tryton-modules-account-es (7.0.2-3) unstable; urgency=medium

  * Update the Depends for 7.0.
  * Update the test depends for 7.0.
  * Update the Depends for 7.0.
  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:19 +0200

tryton-modules-account-es (7.0.2-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:18 +0200

tryton-modules-account-es (7.0.2-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083821).
  * Merging upstream version 7.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 20:01:03 +0200

tryton-modules-account-es (6.0.4-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Switch to pgpmode=none in the watch file.
  * Merging upstream version 6.0.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Jul 2024 10:29:32 +0200

tryton-modules-account-es (6.0.3-1) unstable; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.
  * Merging upstream version 6.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 17 Jan 2023 19:20:50 +0100

tryton-modules-account-es (6.0.2-3) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:52:41 +0100

tryton-modules-account-es (6.0.2-2) unstable; urgency=medium

  * Add missing autopkgtest Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 26 Oct 2021 12:20:30 +0200

tryton-modules-account-es (6.0.2-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.2.
  * Use same debhelper compat as for server and client.
  * Update Depends for 6.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:12:05 +0200

tryton-modules-account-es (5.0.5-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.5.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 10:35:54 +0200

tryton-modules-account-es (5.0.4-1) unstable; urgency=medium

  * Merging upstream version 5.0.4.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 11:09:55 +0200

tryton-modules-account-es (5.0.3-1) unstable; urgency=medium

  * Merging upstream version 5.0.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Dec 2019 10:43:38 +0100

tryton-modules-account-es (5.0.2-2) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:16:38 +0200

tryton-modules-account-es (5.0.2-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 13:10:56 +0100

tryton-modules-account-es (5.0.1-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:34:52 +0100

tryton-modules-account-es (5.0.1-1) unstable; urgency=medium

  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 24 Nov 2018 23:55:20 +0100

tryton-modules-account-es (5.0.0-1) unstable; urgency=medium

  * Initial packaging (Closes: #913168).

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 22 Nov 2018 17:37:14 +0100
